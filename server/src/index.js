import express from 'express';
import fs from 'fs';
/*
// Raspberry Pi (Konnte leider nicht getestet werden, da Server auf dem Pi nicht lief)
const raspi = require('raspi');
const gpio = require('raspi-gpio');
const I2C = require('raspi-i2c').I2C;

raspi.init(() => {
    const output_light = new gpio.DigitalOutput('P1-28');
    const output_blinds = new gpio.DigitalOutput('P1-29');
    output.write(input.read());

    const i2c = new I2C();
);
*/
const app = express();
const port = 3000;

let manualsettings = [
  {
    light: false,
    blinds: false
  }
];

let measurements = [
  {
    temperature: 10.0,
    density: 100
  }
];

let automaticsettings = [
  {
    TNLightLow: '08:00',
    TNLightHigh: '21:00',
    TNLuxLow: '1000',
    TNLuxHigh: '85000',

    HDLightLow: '08:00',
    HDLightHigh: '21:00',
    HDLuxLow: '1000',
    HDLuxHigh: '85000',

    checkTN: false,
    checkHD: false
  }
];

app.use((_, res, next) => {
  console.log('Middleware');
  res.setHeader('access-control-allow-origin', '*');
  res.setHeader(
    'access-control-allow-headers',
    'Origin, X-Requested-With, Content-Type'
  );
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST');
  return next();
});

app.use(express.json());

app.get('/', (req, res) => {
  res.send('hello');
});

//---------------------------------------------------------------------------------------
// Measurements 
//---------------------------------------------------------------------------------------
app.get('/api/get-measurements', (req, res) => {
  
  /* I2C Sensoren Auslesen
  var temperature_sensor = i2c.readByteSync(0x48); // Read Temperatursensor
  var light_sensor = i2c.readByteSync(0x48); // Lichtsensor auslesen

  const body = {temperature: temperature_sensor, density: light_sensor};
  */
  const body = {temperature: (Math.floor(Math.random() * 60)-20), density: (Math.floor(Math.random() * 88000))};
  measurements = JSON.parse(fs.readFileSync('./measurements.json'));
  measurements.shift();
  measurements.push(body);
  fs.writeFileSync('./measurements.json', JSON.stringify(measurements));
  console.log('body', body);
  res.send(measurements);
});

app.get('/api/get-measurements_data', (req, res) => {
  res.send(measurements);
});

//---------------------------------------------------------------------------------------
// Manual Settings
//---------------------------------------------------------------------------------------
app.get('/api/get-manual', (req, res) => {
  res.send(manualsettings);
});

app.post('/api/write-manual', (req, res) => {
  const body = req.body;
  manualsettings = body;
  /* Setzen der GPIOs
  if (manualsettings.light == false) {
    output_light.write(gpio.LOW);
  } else {
    output_light.write(gpio.HIGH);
  }
  if (manualsettings.blinds == false) {
    output_blinds.write(gpio.LOW);
  } else {
    output_blinds.write(gpio.HIGH);
  }
  */
  fs.writeFileSync('./manualsettings.json', JSON.stringify(manualsettings));
  console.log('body', body);
  res.send({ status: 'ok' });
});

//---------------------------------------------------------------------------------------
// Automatic Settings
//---------------------------------------------------------------------------------------
app.get('/api/get-automatic', (req, res) => {
  res.send(automaticsettings);
});

app.post('/api/write-automatic', (req, res) => {
  const body = req.body;
  automaticsettings = body;
  fs.writeFileSync('./automaticsettings.json', JSON.stringify(automaticsettings));
  console.log('body', body);
  res.send({ status: 'ok' });
});

app.listen(port, () => {
  console.log(`Homecontrol available on ${port}`);
  manualsettings = JSON.parse(fs.readFileSync('./manualsettings.json'));
  automaticsettings = JSON.parse(fs.readFileSync('./automaticsettings.json'));
  measurements = JSON.parse(fs.readFileSync('./measurements.json'));

});
