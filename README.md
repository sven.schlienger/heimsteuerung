# wlw-Projekt Sven Schlienger und Michael Jetzer

## Abstract

</p>Bei diesem Projekt ging es darum eine Heimsteuerung zu realisieren, welche mehrere Funktionen vereint. Die Appliaktion umfasst einen manuellen und einen automatischen Modus, welche jeweils umgeschaltet werden können. Alle Funktionen werden über einen Raspberry-Pi gesteuert, wobei die Eingangswerte über I2C eingelesen und die Anzeigeelemente über GPIOs gesetzt werden.</p>  
</br>

## Funktion der Applikation

</p>Die Heimsteuerung hat zwei Modis, welche jeweils Storen und Licht steuern können. Es gibt einen manuellen und automatischen Modus. Zusätzlich werden die Temperatur sowie die Lichtintensität gemessen und in Diagrammen angezeigt.</p>
</br>

### Funktionsdiagramm

![alt text](/Pictures/Funktionsdiagramm_wlw_Projekt.png)
</br>

## Client
</p>Der Client besteht aus drei Files und zwei Modis. Die Funktionalität der Files "index.html", "style.css" und "main.js" werden hier beschrieben.</p>
</br>

### Webseitendarstellung
![alt text](/Pictures/Heimsteuerung_Manuell.png)
</br>
![alt text](/Pictures/Heimsteuerung_Automatisch.png)

### index.html

</p>Im "index.html" wird im "head" die Verlinkung der Files, sowie die Einstellungen des User-Interfaces gemacht. Im "body" werden die Komponenten der beiden Modis "manuell" und "automatisch" erstellt. Der "Client" besitzt verschiedene Komponenten wie "tabs", "buttons" und "charts".</p>
</br>

### style.css

</p>Im "style.css" werden die einzelnen Komponenten in einem "flex"-Grid angeordnet. Es ist ein Normal- und Handymodus implementiert, welche sich an der Anzeigegrösse anpassen (Die Limite liegt bei 600px). Zuoberst im File ist die Schriftart definiert und danach die Hintergrundfarbe für die jeweligen Modis. Des weiteren ist die Anordnung des Grids festgelegt und am Schluss die Konfiguration der Tabs und Sliders.</p>
</br>

### main.js

#### window.onload

</p>Wird ausgeführt beim ersten Laden oder Aktualisieren der Webseite und zeigt standardmässig den manuellen Tab an. refresh und updateMeasurement werden ausgeführt.</p>
</br>

#### openTab

</p>Zuständig für das Umschalten der Tabs.</p>
</br>

#### getMeasurement

</p>Fordert die Messdaten vom Server an ohne eine Messung zu starten. Es werden immer 48 Messpunkte geladen. updateChart wird ausgeführt.</p>
</br>

#### refresh

</p>Fordert die manuellen und automatischen Einstellungen vom Server an und aktualisiert diese auf der Webseite.</p>
</br>

#### updateMeasurement

</p>Startet eine Messung der I2C-Sensoren und fordert die Messdaten vom Server an. Es werden immer 48 Messpunkte geladen. updateChart wird ausgeführt.</p>
</br>

#### updateChart

</p>Aktualisiert die Anzeigewerte und Diagramme für Licht und Temperatur im manuellen und automatischen Modus.</p>
</br>

#### checkTN / checkHD

</p>Die beiden Funktionen garantieren, dass nur ein automatisches Profil aktiv ist. setAutomaticSettings und getMeasurement werden ausgeführt.</p>
</br>

#### SetManualSettings / SetAutomaticSettings

</p>Die beiden Funktionen schicken die Einstellungen des manuellen bzw. automatischen Modus an den Server. Sie werden bei einer Eingabeänderung ausgeführt. </p>
</br>

## Server

</p>Am Anfang des Servers sind die JSON-Speicherstrukturen mit den dazugehötigen Variablen zu finden, danach ist der "express" Zugriff für JSON programmiert. Der Server besitzt insgesammt 6 "API-Enspoints" welche nun erklärt werden.</p>
</br>

### get-measurements

</p>In diesem Endpoint werden die I2C Sensoren über den Raspberry-Pi ausgelesen. Dabei wird zuerst das JSON-File gelesen und das älteste Element gelöscht. Danach werden die neuen Daten dem Body hinzugefügt und dem Client geschickt.</p>
</br>

### get-measurements_data

</p>Diesen Endpoint braucht es nur beim Aufstarten oder Aktualisieren des User-Interfaces. Er wird benötigt, um die Daten vom JSON File zu holen, zu speichern und dem Client zu schicken.</p>
</br>

### get-manual

</p>Diesen Endpoint braucht es nur beim Aufstarten oder Aktualisieren des User-Interfaces. Er wird benötigt, um die Daten vom JSON File zu holen, zu speichern und dem Client zu schicken.</p>
</br>

### write-manual

</p>Bei diesem Endpoint werden die Daten über einen Request vom Client erhalten und gespeichert. Ausserdem werden über diesen Endpoint die Storen hoch- und runterfahren, sowie das Licht ein- und ausgeschaltet. Dies würde über das I2C-Interface auf dem Raspberry-Pi ausgeführt werden.</p>
</br>

### get-automatic

</p>Diesen Endpoint braucht es nur beim Aufstarten oder Aktualisieren des User-Interfaces. Er wird benötigt, um die Daten vom JSON File zu holen, zu speichern und dem Client zu schicken.</p>
</br>

### write automatic

</p>Bei diesem Endpoint werden wie beim "write-manual" Endpoint die Daten über einen Request vom Client erhalten und gespeichert. Jedoch wird hierbei nicht über den Raspberry-Pi angezeigt oder ausgegeben. Das könnte theoretisch noch ausgebaut werden, das automatisch der Status der Storen und des Lichts am Raspberry-Pi angezeigt wird.</p>
</br>

## Inbetriebnahme der Applikation

</p>Um die Applikation in Betrieb nehmen zu können, muss zuerst der Server in via "yarn start" gestartet werden und der Raspbery-Pi eingeschaltet werden. Es muss darauf geachtet werden, dass die Peripherie entsprechend angebunden ist.
Danach muss das "index-html" File in einem Browser geöffnet werden. Nun hat man das User-Interface geöffnet und man kann sich für einen der beiden Modus entscheiden.</p>

### Diverses

</p>Yarn konnte gestartet auf dem Raspberry-Pi werden, allerdings gab es Probleme mit den IP-Port Zugriff, welches nicht in der Zeit gelöst werden konnte. Der Code zur Ansteuerung der GPIOs und I2C ist daher noch nicht getestet worden. Somit werden keine echten Daten ein- bzw. ausgegeben. Die Daten in den Diagrammen sind random-Werte, welche zur Verifikation der Applikation genutzt wurden.</p>
