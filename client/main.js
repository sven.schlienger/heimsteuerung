//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// Start der Webseite oder manuelles Aktualisieren
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

// Bein Webseitenstart oder Aktualisieren
// refresh fordert die manuellen und automatischen Einstellungen vom Server
// getMeasurement fordert die Messdaten vom Server an, ohne eine neue Messung zu starten
window.onload = () => {
  document.getElementById("defaultOpen").click();
  refresh();
  getMeasurement();
}

// Funktion für Tabs
function openTab(evt, TabName) {
  var i, tabcontent, tablinks;

  // Alle Elemente mit der Klasse tabcontent einlesen
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Alle Elemente der Klasse "tablinks" und aktive Klasse entfernen
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Anzeigen des aktiven Tabs
  document.getElementById(TabName).style.display = "block";
  evt.currentTarget.className += " active";
} 

//---------------------------------------------------------------------------------------
// Fordert die Messdaten vom Server an ohne eine Messung zu starten
function getMeasurement() {
  const result_measurement = fetch("http://localhost:3000/api/get-measurements_data");

  // callback registrieren, wenn result returned
  result_measurement.then((httpRes) => {
    // resultat des Requests ist hier als param erhältlich.
    console.log('res', httpRes);

    // JSON muss geparsed werden, hierzu gibt es auch wieder ein Promise
    const jsonPromise = httpRes.json();
    jsonPromise.then((json) => {

      // endlich haben wir das resultat!!
      updateChart(json);
    });
  });
}

//---------------------------------------------------------------------------------------
// Fordert die Einstellungen vom Server an und aktualisiert diese auf der Webseite
function refresh() {
  const result_manual = fetch("http://localhost:3000/api/get-manual");

  // callback registrieren, wenn result returned
  result_manual.then((httpRes) => {
    // resultat des Requests ist hier als param erhältlich.
    console.log('res', httpRes);

    // JSON muss geparsed werden, hierzu gibt es auch wieder ein Promise
    const jsonPromise = httpRes.json();
    jsonPromise.then((json) => {

      // endlich haben wir das resultat!!
      document.getElementById("checkLight").checked = json.light;
      document.getElementById('checkBlinds').checked = json.blinds;
    });
  });

  const result_automatic = fetch("http://localhost:3000/api/get-automatic");

  // callback registrieren, wenn result returned
  result_automatic.then((httpRes) => {
    // resultat des Requests ist hier als param erhältlich.
    console.log('res', httpRes);

    // JSON muss geparsed werden, hierzu gibt es auch wieder ein Promise
    const jsonPromise = httpRes.json();
    jsonPromise.then((json) => {

      // endlich haben wir das resultat!!
      document.getElementById("TNTimeLow").value = json.TNTimeHigh;
      document.getElementById("TNTimeHigh").value = json.TNTimeHigh;
      document.getElementById("TNLuxLow").value = json.TNLuxLow;
      document.getElementById("TNLuxHigh").value = json.TNLuxHigh; 
      document.getElementById("HDTimeLow").value = json.HDTimeLow;
      document.getElementById("HDTimeHigh").value = json.HDTimeHigh;
      document.getElementById("HDLuxLow").value = json.HDLuxLow; 
      document.getElementById("HDLuxHigh").value = json.HDLuxHigh;
      document.getElementById("checkTN").checked = json.checkTN;
      document.getElementById("checkHD").checked = json.checkHD;
    });
  });
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// Start der Webseite oder manuelles Aktualisieren
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

// Setzen des Messintervalls um die Webseite zu updaten (Intervall in ms))
setInterval(() => {
  updateMeasurement();
},5000);

//---------------------------------------------------------------------------------------
// Startet eine Messung der I2C-Sensoren und fordert die Messdaten vom Server an
function updateMeasurement() {

  const result_measurement = fetch("http://localhost:3000/api/get-measurements");

  // callback registrieren, wenn result returned
  result_measurement.then((httpRes) => {
    // resultat des Requests ist hier als param erhältlich.
    console.log('res', httpRes);

    // JSON muss geparsed werden, hierzu gibt es auch wieder ein Promise
    const jsonPromise = httpRes.json();
    jsonPromise.then((json) => {

      // endlich haben wir das resultat!!
      updateChart(json);
    });
  });
};

//---------------------------------------------------------------------------------------
// Aktualisiert die Anzeigewerte für Licht und Temperatur im manuellen und automatischen Modus
// Aktualisiert die Diagramme für Licht und Temperatur im manuellen und automatischen Modus
function updateChart(messwerte) {
  document.getElementById("lightintense_value").innerHTML = String(messwerte[messwerte.length-1].density) + " lux";
  document.getElementById("temperature_value").innerHTML = String(messwerte[messwerte.length-1].temperature) +" °C";

  document.getElementById("lightintense_value_a").innerHTML = String(messwerte[messwerte.length-1].density) + " lux";
  document.getElementById("temperature_value_a").innerHTML = String(messwerte[messwerte.length-1].temperature) +" °C";

// Definieren der Arrays für die Diagramme
  var xValues = new Array(messwerte.length);

  var yDensity = new Array(messwerte.length);
  var yDensityMax = new Array(messwerte.length);
  var yDensityMin = new Array(messwerte.length);

// Speichert Lichtdaten in die entsprechenden Array-Felder ab
  for (var i = 0; i < yDensity.length; i++) {
    xValues[i] = 12-i/4;
    yDensity[i] = messwerte[i].density;
    if(document.getElementById("checkTN").checked) {
      yDensityMax[i] = document.getElementById("TNLuxHigh").value;
      yDensityMin[i] = document.getElementById("TNLuxLow").value;
    } else if (document.getElementById("checkHD").checked) {
      yDensityMax[i] = document.getElementById("HDLuxHigh").value;
      yDensityMin[i] = document.getElementById("HDLuxLow").value;
    } else {
      yDensityMax[i] = null;
      yDensityMin[i] = null;
    }
  }

// Licht Diagramm im manuellen Modus
  new Chart("LightChart", {
    type: "line",
    data: {
      labels: xValues,
      datasets: [{
        data: yDensity,
        borderColor: "blue",
        fill: false
      }]
    },
    options: {
      legend: {display: false},
      scales: {
        yAxes: [{ticks: {min: 0, max:88000}}],
      },
      animation: false
    }
  });

  // Licht Diagramm im automatischen Modus
  new Chart("LightChartA", {
    type: "line",
    data: {
      labels: xValues,
      datasets: [{ 
        data: yDensityMin,
        borderColor: "red",
        fill: false
      }, { 
        data: yDensityMax,
        borderColor: "red",
        fill: false
      }, { 
        data: yDensity,
        borderColor: "blue",
        fill: false
      }]
    },
    options: {
      legend: {display: false},
      scales: {
        yAxes: [{ticks: {min: 0, max:88000}}],
      },
      animation: false
    }
  });

  // Speichert Temperaturdaten in die entsprechenden Array-Felder ab
  var yTemp = new Array(messwerte.length);

  for (var i = 0; i < yDensity.length; i++) {
    yTemp[i] = messwerte[i].temperature;
  }

  // Temperatur Diagramm im manuellen Modus
  new Chart("TempChart", {
    type: "line",
    data: {
      labels: xValues,
      datasets: [{
        data: yTemp,
        borderColor: "blue",
        fill: false
      }]
    },
    options: {
      legend: {display: false},
      scales: {
        yAxes: [{ticks: {min: -20, max:40}}],
      },
      animation: false
    }
  });
  
  // Temperatur Diagramm im manuellen Modus        
  new Chart("TempChartA", {
    type: "line",
    data: {
      labels: xValues,
      datasets: [{
        data: yTemp,
        borderColor: "blue",
        fill: false
      }]
    },
    options: {
      legend: {display: false},
      scales: {
        yAxes: [{ticks: {min: -20, max:40}}],
      },
      animation: false
    }
  });      
};

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// Start der Webseite oder manuelles Aktualisieren
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

// Wenn Checkbox Tag-/Nacht gedrückt wird...
function checkTN(check) {
  if (check.checked) {
    document.getElementById("checkHD").checked = false;
  } 
  SetAutomaticSettings();
  getMeasurement();
}

//---------------------------------------------------------------------------------------
// Wenn Checkbox Ferien gedrückt wird...
function checkHD(check) {
  if (check.checked) {
    document.getElementById("checkTN").checked = false;
  } 
  SetAutomaticSettings();
  getMeasurement();
}

//---------------------------------------------------------------------------------------
// Schicken der manuellen Einstellungen an den Server
async function SetManualSettings() {
  payload = {'light': document.getElementById("checkLight").checked, 'blinds': document.getElementById('checkBlinds').checked};
  const response = await fetch('http://localhost:3000/api/write-manual', {
    method: 'POST',
    headers: {
        'Content-Type' : 'application/json'
    },
    body: JSON.stringify(payload)
  });
  const json = await response.json();
  console.log('response', response);
}

//---------------------------------------------------------------------------------------
// Schicken der automatischen Einstellungen an den Server
async function SetAutomaticSettings() {
  payload = {'TNTimeLow': document.getElementById("TNTimeLow").value, 
             'TNTimeHigh': document.getElementById("TNTimeHigh").value,
             'TNLuxLow': document.getElementById("TNLuxLow").value,
             'TNLuxHigh': document.getElementById("TNLuxHigh").value, 
             'HDTimeLow': document.getElementById("HDTimeLow").value,
             'HDTimeHigh': document.getElementById("HDTimeHigh").value,
             'HDLuxLow': document.getElementById("HDLuxLow").value, 
             'HDLuxHigh': document.getElementById("HDLuxHigh").value,
             'checkTN': document.getElementById("checkTN").checked,
             'checkHD': document.getElementById("checkHD").checked
            };
  const response = await fetch('http://localhost:3000/api/write-automatic', {
    method: 'POST',
    headers: {
        'Content-Type' : 'application/json'
    },
    body: JSON.stringify(payload)
   });
  const json = await response.json();
  console.log('response', response);
  updateMeasurement();
}